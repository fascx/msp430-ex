;*******************************************************************************
;                MSP430G2553
;             -----------------
;         /|\|              XIN|-
;          | |                 |
;          --|RST          XOUT|-
;       __   |                 |
;   |--o  o--| P1.4-i    P1.0-o|-->LED
;
;*******************************************************************************

#include <msp430.h>
#define PC r0
#define SP r1
#define SR r2


;-------------------------------------------------------------------------------
;            .data                              ; RAM variables
;------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;            .bss                               ; RAM Static variables
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;            .text                              ; Program Start
;------------------------------------------------------------------------------

RESET:		mov.w   #__stack,SP             ; Initialize stackpointer (SP == R1)
StopWDT:	mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop WDT
SetupP1:	bis.b   #BIT0,&P1DIR            ; Set P1.0 output
			mov.b   #BIT3,&P1OUT            ; P1.4 set, else reset
			bis.b   #BIT3,&P1REN            ; P1.4 pullup

	Mainloop:	bit.b   #BIT3,&P1IN         ; P1.4 hi/low?
				jc      ON                  ; jmp--> P1.4 is set

	OFF:		bic.b   #BIT0,&P1OUT        ; P1.0 = 0 / LED OFF
				jmp     Mainloop            ;
	ON:			bis.b   #BIT0,&P1OUT        ; P1.0 = 1 / LED ON
				jmp     Mainloop            ;

;------------------------------------------------------------------------------
NON_ISR:;   Unexpected ISR handler
;-------------------------------------------------------------------------------
            reti

;------------------------------------------------------------------------------
;           Interrupt Vectors
;------------------------------------------------------------------------------
            .section ".vectors", "ax", @progbits
            .word NON_ISR    ;0xffe0 slot 0
            .word NON_ISR    ;0xffe2 slot 1
            .word NON_ISR    ;0xffe4 slot 2
            .word NON_ISR    ;0xffe6 slot 3
            .word NON_ISR    ;0xffe8 slot 4
            .word NON_ISR    ;0xffea slot 5
            .word NON_ISR    ;0xffec slot 6
            .word NON_ISR    ;0xffee slot 7
            .word NON_ISR    ;0xfff0 slot 8
            .word NON_ISR    ;0xfff2 slot 9
            .word NON_ISR    ;0xfff4 slot 10
            .word NON_ISR    ;0xfff6 slot 11
            .word NON_ISR    ;0xfff8 slot 12
            .word NON_ISR    ;0xfffa slot 13
            .word NON_ISR    ;0xfffc slot 14
            .word RESET      ;0xfffe slot 15
            .end
